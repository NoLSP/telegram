package com.example.telegram.ui.objects

import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import androidx.drawerlayout.widget.DrawerLayout
import com.example.telegram.R
import com.example.telegram.ui.fragments.SettingsFragment
import com.example.telegram.utilits.replaceFragment
import com.mikepenz.materialdrawer.AccountHeader
import com.mikepenz.materialdrawer.AccountHeaderBuilder
import com.mikepenz.materialdrawer.Drawer
import com.mikepenz.materialdrawer.DrawerBuilder
import com.mikepenz.materialdrawer.model.DividerDrawerItem
import com.mikepenz.materialdrawer.model.ProfileDrawerItem
import com.mikepenz.materialdrawer.model.interfaces.IDrawerItem

class AppDrawer (val mainActivity: AppCompatActivity, val toolbar: Toolbar){

    private lateinit var drawer: Drawer
    private lateinit var header: AccountHeader
    private lateinit var drawerLayout : DrawerLayout

    fun create(){
        createHeader()
        createDrawer()
        drawerLayout = drawer.drawerLayout
    }

    fun disableDrawer(){
        drawer.actionBarDrawerToggle?.isDrawerIndicatorEnabled = false
        mainActivity.supportActionBar?.setDisplayHomeAsUpEnabled(true)
        drawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED)
        toolbar.setNavigationOnClickListener {
            mainActivity.supportFragmentManager.popBackStack()
        }
    }

    fun enableDrawer(){
        mainActivity.supportActionBar?.setDisplayHomeAsUpEnabled(false)
        drawer.actionBarDrawerToggle?.isDrawerIndicatorEnabled = true
        drawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_UNLOCKED)
        toolbar.setNavigationOnClickListener {
            drawer.openDrawer()
        }
    }

    private fun createDrawer() {
        drawer = DrawerBuilder()
            .withActivity(mainActivity)
            .withToolbar(toolbar)
            .withActionBarDrawerToggle(true)
            .withSelectedItem(-1)
            .withAccountHeader(header)
            .addDrawerItems(
                ProfileDrawerItem().withIdentifier(100)
                    .withIcon(R.drawable.ic_menu_create_groups)
                    .withName("Создать группу")
                    .withSelectable(false),
                ProfileDrawerItem().withIdentifier(101)
                    .withIcon(R.drawable.ic_menu_secret_chat)
                    .withName("Создать секретный чат")
                    .withSelectable(false),
                ProfileDrawerItem().withIdentifier(102)
                    .withIcon(R.drawable.ic_menu_create_channel)
                    .withName("Создать канал")
                    .withSelectable(false),
                ProfileDrawerItem().withIdentifier(103)
                    .withIcon(R.drawable.ic_menu_contacts)
                    .withName("Контакты")
                    .withSelectable(false),
                ProfileDrawerItem().withIdentifier(104)
                    .withIcon(R.drawable.ic_menu_phone)
                    .withName("Звонки")
                    .withSelectable(false),
                ProfileDrawerItem().withIdentifier(105)
                    .withIcon(R.drawable.ic_menu_favorites)
                    .withName("Избранное")
                    .withSelectable(false),
                ProfileDrawerItem().withIdentifier(106)
                    .withIcon(R.drawable.ic_menu_settings)
                    .withName("Настройки")
                    .withSelectable(false),
                DividerDrawerItem(),
                ProfileDrawerItem().withIdentifier(107)
                    .withIcon(R.drawable.ic_menu_invate)
                    .withName("Пригласить друзей")
                    .withSelectable(false),
                ProfileDrawerItem().withIdentifier(108)
                    .withIcon(R.drawable.ic_menu_help)
                    .withName("Вопросы о телеграм")
                    .withSelectable(false)
            ).withOnDrawerItemClickListener(object : Drawer.OnDrawerItemClickListener{
                override fun onItemClick(
                    view: View?,
                    position: Int,
                    drawerItem: IDrawerItem<*>
                ): Boolean {
                    when(position){
                        7 -> mainActivity.replaceFragment(SettingsFragment())
                    }
                    return false
                }
            }).build()
    }

    private fun createHeader() {
        header = AccountHeaderBuilder()
            .withActivity(mainActivity)
            .withHeaderBackground(R.drawable.header)
            .addProfiles(
                ProfileDrawerItem().withName("Artem")
                    .withEmail("+79517925000")
            ).build()
    }
}